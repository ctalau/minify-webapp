var fs = require('fs')
var UglifyJS = require("uglifyjs");

var content = fs.readFileSync('workspace-v19.0.0.js', 'utf8');

console.log(content.length, ' - initial');

/**
 * 1. Mangle statics & constructors
 * 
 * This replaces the 'namespace' notation like 'sync.util.logError' which 
 * causes may object propery accesses with 'sync_util_logError_' notation.
 */
// Collect statics definitions
var statics = new Map;
mangledContent = content.replace(/((goog)|(sync))(\.[a-zA-Z0-9_]+)+(=?)/g, function(match) {
  if (match.indexOf('prototype') !== -1 || 
    match.indexOf('superClass_') !== -1 ) {
    return match;
  }
  
  var mangled = match;
  if (arguments[5] === '=') {
    let symbol = match.substring(0, match.length - 1).replace(/\./g, '_');
    statics.set(symbol, 1);
    mangled = symbol + '_=';
  }
  return mangled;
});
// Mangle every occurrence of a static field.
mangledContent = mangledContent.replace(/((goog)|(sync))(\.[a-zA-Z0-9_]+)+/g, function(match) {
  // try to find at least a prefix that is recorded as a symbol
  let symbol = match.replace(/\./g, '_');
  while (symbol !== '' && !statics.has(symbol)) {
    let matchIndex = symbol.lastIndexOf('_', symbol.length - 1);
    symbol = symbol.substring(0, matchIndex);
  }

  // mangle that symbol
  if (statics.has(symbol)) {
    let crt = statics.get(symbol) || 0;
    statics.set(symbol, crt + 1);
    return symbol + '_' + match.substring(symbol.length);
  }
  return match;
})
var staticDefs = 'var ' + Array.from(statics.keys()).map((s) => s + '_').join(',\n') + ';'; 
content = staticDefs + '\n' + mangledContent;

// TODO: 2. dead-code removal (w/ exceptions)
content = `(function(){ ${content} })()`;
// get rid of eval() invocations that prevent optimizations. They are harmless anyway.
content = content.replace(/.eval\(/g, function(match) {
  if (match[0] !== '.') {
    return match[0] + 'goog_global_.eval(';
  }
  return match;
});
// XXX: uglifyjs input.js --compress "dead_code,unused,passes=10" 

// TODO: 3. export the rest of the symbols
// TODO: 4. mangle
// XXX: uglifyjs input.js --mangle --mangle-props --mangle-regex="/_$/" 

fs.writeFileSync('workspace-v2.js', content);
