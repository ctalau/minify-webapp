Manger for the Web Author project
=================================

Conventions
-----------

1. Classes have a name like:
```
  sync.package.ClassName
  goog.package.ClassName
```
2. Static class methods have a name like:
```
  sync.package.ClassName.method
```
3. Static package method are stored in several packages and have a name like
```
  sync.package.method
```
4. Methods and fields that are private or protected, and thus can be mangledend in a '_'.
```
  this.className_
```
