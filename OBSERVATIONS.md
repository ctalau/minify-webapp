Observations
============


1. Big chunks of bloat
 293645 dates.txt       (lines related to date formatting)
 110642 msgs.txt        (i18n messages of the UI)
1453829 workspace-v1.js (... the rest)

2. mangle statics & classes & private members

3. Libs:
- blocking:
 - jquery  - we need only parseHTML
 - layout  - use flexbox on new browsers
 - jqm     - no need
lazy:
 - soy     - no need
 - react   - preact / vue / nothing
 - select2 - ok

4. Split code in two modules
